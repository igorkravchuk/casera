$(function() {
    $('form').submit(function() {
        var $form = $(this);

        if ($form.valid()) {
            var formData = $form.serialize();
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: formData,
                success: function(data) {
                    $('#contactsDrawer').removeClass('show');
                    $('#subForm').trigger('reset');
                    setTimeout(function() { $('#myModal').modal(); }, 500);

                },
                error: function(xhr, str) {
                    console.log('Error: ' + xhr.responseCode);
                }
            });
        }

        return false;
    })
})