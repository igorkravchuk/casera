$(function() {
    

    // $('.hero-month-item').each(function() {
    //     var $item = $(this);
        
    //     $item.find('.hero-month-point').hover(function() {
    //         $item.addClass('active');
    //     }, function() {
    //         $item.removeClass('active');
    //     })
    // });


    var currentItem;
    var timerSize = 8000;
    var timer;

    var listItems = $('.js-hero-month')
        .each(function() {
            var $item = $(this);
            
            $item.find('.hero-month-point').bind('click', function() {
                $(currentItem).removeClass('active');
                currentItem = $item[0];
                $(currentItem).addClass('active');

                clearTimeout(timer);
                timer = setTimeout(nextMonth, timerSize)
            });
        })
        .toArray();
    
    function nextMonth() {
        $(currentItem).removeClass('active');

        var currentIndex = listItems.indexOf(currentItem);
        if (!currentItem || currentIndex === listItems.length - 1) {
            currentItem = listItems[0];
        } else {
            currentItem = listItems[currentIndex+1];
        }
        $(currentItem).addClass('active');

        timer = setTimeout(nextMonth, timerSize)
    }
    

    var $firstItem = $('.hero-month-item:not(.js-hero-month)');

    setTimeout(function() {
        $firstItem.addClass('active');
        $firstItem.find('.hero-month-point').addClass('hover');
    }, 500)
    setTimeout(function() {
        $firstItem.removeClass('active');
        nextMonth();
    }, 2000)
})