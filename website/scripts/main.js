$(function() {
    var $body = $('body'); 
    var $navbar = $('#navbar');   

    function topScroll() {
        if ($(window).scrollTop() <= 60) {
            $navbar.removeClass('navbar-pops-up');
        } else {
            $navbar.addClass('navbar-pops-up');
        }
    }

    topScroll();

    $(window).scroll(function() {
        topScroll()
    })

    

    $('[data-drawer]').bind('click', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-drawer');
        var $target = $(target)
        console.log($target);
        $target.toggleClass('show');
        return false;
    })

    $('[data-page-scroll]').bind('click', function(event) {
        var target = $(this).attr('data-page-scroll') || $(this).attr('href');
        var $target = $(target)
        var $navbar = $('.navbar-fixed-top');

        if (!$target.length) {
            window.location.href = '/' + target;
        }

        var scrollTo = $target.offset().top - ($navbar.length ? $navbar.height() : 0);

        if ($target.length) {
            $('html, body').stop().animate({
                scrollTop: (scrollTo)
            }, 1250, 'easeInOutExpo');
        }
        event.preventDefault();
        return false;
    });

    var path = location.pathname.split('/');
    var fileName = path[path.length -1];

    $('#main-navbar .nav li').each(function() {
        var $item = $(this);
        var $link = $item.find('a');
        
        if ($link.attr('href') == fileName) {
            $item.addClass('active');
        }
    });

});